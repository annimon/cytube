var Logger = require("./logger");
var Config = require("./config");
var spawn = require("child_process").spawn;
var https = require("https");
var http = require("http");
var urlparse = require("url");
var path = require("path");
require("status-message-polyfill");

var USE_JSON = true;
var TIMEOUT = 30000;

var acceptedCodecs = {
    "mov/h264": true,
    "flv/h264": true,
    "matroska/vp8": true,
    "matroska/vp9": true,
    "ogg/theora": true
};

var acceptedAudioCodecs = {
    "mp3": true,
    "vorbis": true
};

var audioOnlyContainers = {
    "mp3": true
};

function fflog() { }

function initFFLog() {
    if (fflog.initialized) return;
    var logger = new Logger.Logger(path.resolve(__dirname, "..", "ffmpeg.log"));
    fflog = function () {
        logger.log.apply(logger, arguments);
    };
    fflog.initialized = true;
}

function fixRedirectIfNeeded(urldata, redirect) {
    if (!/^https?:/.test(redirect)) {
        redirect = urldata.protocol + "//" + urldata.host + redirect;
    }

    return redirect;
}

function translateStatusCode(statusCode) {
    switch (statusCode) {
        case 400:
            return "Запрос ссылки на аудио/видео отклонён как неверный.  " +
                "Обратитесь в поддержку для решения проблемы.";
        case 401:
        case 403:
            return "Нет доступа к ссылке.  Обратитесь к владельцу " +
                "сайта, на котором размещён файл, для получения разрешения на скачивание.";
        case 404:
            return "Запрашиваемыя ссылка не найдена (404).";
        case 500:
        case 503:
            return "На сервере сайта, размещающем файл, произошла ошибка и " +
                "выполнить запрос невозможно.  Попробуйте позже и, " +
                "если проблема повторится, обратитесь к владельцу сайта.";
        default:
            return "Неизвестная ошибка при запросе ссылки на аудио/видео.  " +
                "Обратитесь в поддержку для решения проблемы.";
    }
}

function testUrl(url, cb, redirCount) {
    if (!redirCount) redirCount = 0;
    var data = urlparse.parse(url);
    if (!/https?:/.test(data.protocol)) {
        return cb("Поддерживаются только ссылки, начинающиеся на 'http://' или 'https://'");
    }

    if (!data.hostname) {
        return cb("Отсутствует адрес сайта в ссылке на файл. Запрос не может быть обработан.");
    }

    var transport = (data.protocol === "https:") ? https : http;
    data.method = "HEAD";
    var req = transport.request(data, function (res) {
        req.abort();

        if (res.statusCode === 301 || res.statusCode === 302) {
            if (redirCount > 2) {
                return cb("Запрос на аудио/видео был перенаправлен более, чем два раза. " +
                          "Это может являться причиной неправильной настройки сайта, на " +
                          "котором размещён контент.  Попробуйте воспользоваться прямой " +
                          "ссылкой на файл.  Детали по ссылке https://git.io/vrE75 ");
            }
            return testUrl(fixRedirectIfNeeded(data, res.headers["location"]), cb,
                    redirCount + 1);
        }

        if (res.statusCode !== 200) {
            return cb(translateStatusCode(res.statusCode));
        }

        if (!/^audio|^video/.test(res.headers["content-type"])) {
            return cb("Ожидался тип контента, начинающийся на 'audio' или 'video', но " +
                      "получено '" + res.headers["content-type"] + "'.  Поддерживаются только " +
                      "прямые ссылки на аудио и видео файлы. Сайт, размещающий файл, должен " +
                      "быть настроен на отправку правильного MIME-типа.  " +
                      "Детали по ссылке https://git.io/vrE75 ");
        }

        cb();
    });

    req.on("error", function (err) {
        cb("Неизвестная ошибка при попытке обработать запрос.  " +
           "Попробуйте ещё раз или обратитесь в поддержку для решения проблемы. " +
           (!!err.code ? (" Error code: " + err.code) : ""));
    });

    req.end();
}

function readOldFormat(buf) {
    var lines = buf.split("\n");
    var tmp = { tags: {} };
    var data = {
        streams: []
    };

    lines.forEach(function (line) {
        if (line.match(/\[stream\]|\[format\]/i)) {
            return;
        } else if (line.match(/\[\/stream\]/i)) {
            data.streams.push(tmp);
            tmp = { tags: {} };
        } else if (line.match(/\[\/format\]/i)) {
            data.format = tmp;
            tmp = { tags: {} };
        } else {
            var kv = line.split("=");
            var key = kv[0].toLowerCase();
            if (key.indexOf("tag:") === 0) {
                tmp.tags[key.split(":")[1]] = kv[1];
            } else {
                tmp[key] = kv[1];
            }
        }
    });

    return data;
}

function isAlternateDisposition(stream) {
    if (!stream.disposition) {
        return false;
    }

    for (var key in stream) {
        if (key !== "default" && stream.disposition[key]) {
            return true;
        }
    }

    return false;
}

function reformatData(data) {
    var reformatted = {};

    var duration = parseInt(data.format.duration, 10);
    if (isNaN(duration)) duration = "--:--";
    reformatted.duration = Math.ceil(duration);

    var bitrate = parseInt(data.format.bit_rate, 10) / 1000;
    if (isNaN(bitrate)) bitrate = 0;
    reformatted.bitrate = bitrate;

    reformatted.title = data.format.tags ? data.format.tags.title : null;
    var container = data.format.format_name.split(",")[0];

    var isVideo = false;
    var audio = null;
    for (var i = 0; i < data.streams.length; i++) {
        const stream = data.streams[i];

        // Trash streams with alternate dispositions, e.g. `attached_pic` for
        // embedded album art on MP3s (not a real video stream)
        if (isAlternateDisposition(stream)) {
            continue;
        }

        if (stream.codec_type === "video" &&
                !audioOnlyContainers.hasOwnProperty(container)) {
            isVideo = true;
            if (acceptedCodecs.hasOwnProperty(container + "/" + stream.codec_name)) {
                reformatted.vcodec = stream.codec_name;
                reformatted.medium = "video";
                reformatted.type = [container, reformatted.vcodec].join("/");

                if (stream.tags && stream.tags.title) {
                    reformatted.title = stream.tags.title;
                }

                return reformatted;
            }
        } else if (stream.codec_type === "audio" && !audio &&
                acceptedAudioCodecs.hasOwnProperty(stream.codec_name)) {
            audio = {
                acodec: stream.codec_name,
                medium: "audio"
            };

            if (stream.tags && stream.tags.title) {
                audio.title = stream.tags.title;
            }
        }
    }

    // Override to make sure video files with no valid video streams but some
    // acceptable audio stream are rejected.
    if (isVideo) {
        return reformatted;
    }

    if (audio) {
        for (var key in audio) {
            reformatted[key] = audio[key];
        }
    }

    return reformatted;
}

exports.ffprobe = function ffprobe(filename, cb) {
    fflog("Spawning ffprobe for " + filename);
    var childErr;
    var args = ["-show_streams", "-show_format", filename];
    if (USE_JSON) args = ["-of", "json"].concat(args);
    var child = spawn(Config.get("ffmpeg.ffprobe-exec"), args);
    var stdout = "";
    var stderr = "";
    var timer = setTimeout(function () {
        Logger.errlog.log("Possible runaway ffprobe process for file " + filename);
        fflog("Killing ffprobe for " + filename + " after " + (TIMEOUT/1000) + " seconds");
        childErr = new Error("File query exceeded time limit of " + (TIMEOUT/1000) +
                             " seconds.  To avoid this issue, encode your videos " +
                             "using the 'faststart' option: " +
                             "https://trac.ffmpeg.org/wiki/Encode/H.264#faststartforwebvideo");
        child.kill("SIGKILL");
    }, TIMEOUT);

    child.on("error", function (err) {
        childErr = err;
    });

    child.stdout.on("data", function (data) {
        stdout += data;
    });

    child.stderr.on("data", function (data) {
        stderr += data;
        if (stderr.match(/the tls connection was non-properly terminated/i)) {
            fflog("Killing ffprobe for " + filename + " due to TLS error");
            childErr = new Error("The connection was closed unexpectedly.  " +
                                 "If the problem continues, contact support " +
                                 "for troubleshooting assistance.");
            child.kill("SIGKILL");
        }
    });

    child.on("close", function (code) {
        clearTimeout(timer);
        fflog("ffprobe exited with code " + code + " for file " + filename);
        if (code !== 0) {
            if (stderr.match(/unrecognized option|json/i) && USE_JSON) {
                Logger.errlog.log("Warning: ffprobe does not support -of json.  " +
                                  "Assuming it will have old output format.");
                USE_JSON = false;
                return ffprobe(filename, cb);
            }

            if (!childErr) childErr = new Error(stderr);
            return cb(childErr);
        }

        var result;
        if (USE_JSON) {
            try {
                result = JSON.parse(stdout);
            } catch (e) {
                return cb(new Error("Невозможно обработать вывод ffprobe: " + e.message));
            }
        } else {
            try {
                result = readOldFormat(stdout);
            } catch (e) {
                return cb(new Error("Невозможно обработать вывод ffprobe: " + e.message));
            }
        }

        return cb(null, result);
    });
}

exports.query = function (filename, cb) {
    if (Config.get("ffmpeg.log") && !fflog.initialized) {
        initFFLog();
    }

    if (!Config.get("ffmpeg.enabled")) {
        return cb("Воспроизведение необработанных файлов не включено на сервере");
    }

    if (!filename.match(/^https?:\/\//)) {
        return cb("Воспроизведение необработанных файлов поддерживается только по HTTP- " +
                  "или HTTPS-ссылкам.  Убедитесь, что ссылка начинается на 'http://' или 'https://'");
    }

    testUrl(filename, function (err) {
        if (err) {
            return cb(err);
        }

        exports.ffprobe(filename, function (err, data) {
            if (err) {
                if (err.code && err.code === "ENOENT") {
                    return cb("Невозможно запустить `ffprobe`.  Установите правильное имя " +
                              "исполняемого файла в ffmpeg.ffprobe-exec в файле onfig.yaml.  " +
                              "Если вы используете Debian или Ubuntu, это, вероятно, avprobe.");
                } else if (err.message) {
                    if (err.message.match(/protocol not found/i))
                        return cb("Ссылка использует протокол, неподдерживаемый серверной версией " +
                                  "ffmpeg.  Некоторые старые версии ffprobe/avprobe не поддерживают HTTPS.");

                    if (err.message.match(/exceeded time limit/) ||
                        err.message.match(/closed unexpectedly/i)) {
                        return cb(err.message);
                    }

                    // Ignore ffprobe error messages, they are common and most often
                    // indicate a problem with the remote file, not with this code.
                    if (!/(av|ff)probe/.test(String(err)))
                        Logger.errlog.log(err.stack || err);
                    return cb("Неизвестная ошибка при попытке обработать запрос.  " +
                              "Попробуйте ещё раз или обратитесь в поддержку для решения проблемы. ");
                } else {
                    if (!/(av|ff)probe/.test(String(err)))
                        Logger.errlog.log(err.stack || err);
                    return cb("Неизвестная ошибка при попытке обработать запрос.  " +
                              "Попробуйте ещё раз или обратитесь в поддержку для решения проблемы. ");
                }
            }

            try {
                data = reformatData(data);
            } catch (e) {
                Logger.errlog.log(e.stack || e);
                return cb("Неизвестная ошибка при попытке обработать запрос.  " +
                          "Попробуйте ещё раз или обратитесь в поддержку для решения проблемы. ");
            }

            if (data.medium === "video") {
                data = {
                    title: data.title || "Исходное видео",
                    duration: data.duration,
                    bitrate: data.bitrate,
                    codec: data.type
                };

                cb(null, data);
            } else if (data.medium === "audio") {
                data = {
                    title: data.title || "Исходное аудио",
                    duration: data.duration,
                    bitrate: data.bitrate,
                    codec: data.acodec
                };

                cb(null, data);
            } else {
                return cb("Файл не содержит требуемый кодек.  Детали по ссылке " +
                          "https://git.io/vrE75");
            }
        });
    });
};
