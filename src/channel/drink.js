var ChannelModule = require("./module");

function DrinkModule(channel) {
    ChannelModule.apply(this, arguments);
    this.drinks = 0;
    this.arts = 0;
}

DrinkModule.prototype = Object.create(ChannelModule.prototype);

DrinkModule.prototype.onUserPostJoin = function (user) {
    user.socket.emit("drinkCount", this.drinks);
    user.socket.emit("artCount", this.arts);
};

DrinkModule.prototype.onUserPreChat = function (user, data, cb) {
    var msg = data.msg;
    var perms = this.channel.modules.permissions;
    var isDrink = msg.match(/^\/d-?[0-9]*/);
    var isArt = msg.match(/^\/art-?[0-9]*/);
    if ((isDrink || isArt) && perms.canCallDrink(user)) {
        if (isDrink) msg = msg.substring(2);
        if (isArt) msg = msg.substring(4);
        var m = msg.match(/^(-?[0-9]+)/);
        var count;
        if (m) {
            count = parseInt(m[1]);
            if (isNaN(count) || count < -10000 || count > 10000) {
                return;
            }

            if (isDrink) {
                msg = msg.replace("rink", "выпил"); //костыль для команды /drink
                msg = msg.replace(m[1], "").trim();
                if (msg || count > 0) {
                    msg = count + " " + this.pluralForm(Math.abs(count), "чай", "чая", "чаёв") + "! " + msg;
                } else {
                    this.drinks += count;
                    this.channel.broadcastAll("drinkCount", this.drinks);
                    return cb(null, ChannelModule.DENY);
                }
            }
            if (isArt) {
            	msg = msg.replace(m[1], "").trim();
                if (msg || count > 0) {
                    msg = count + " " + this.pluralForm(Math.abs(count), "рисунок", "рисунка", "рисунков") + "! " + msg;
                } else {
                    this.drinks += count;
                    this.channel.broadcastAll("artCount", this.drinks);
                    return cb(null, ChannelModule.DENY);
                }
            }
        } else {
        	if (isDrink)
        	    msg = msg.trim() + " чай!";
        	if (isArt)
        	    msg = msg.trim() + " рисунок!";
            count = 1;
        }

        if (isDrink) {
            this.drinks += count;
            this.channel.broadcastAll("drinkCount", this.drinks);
        }
        if (isArt) {
        	this.arts += count;
            this.channel.broadcastAll("artCount", this.arts);
        }
        data.msg = msg;
        data.meta.addClass = "drink";
        data.meta.forceShowName = true;
        cb(null, ChannelModule.PASSTHROUGH);
    } else {
        cb(null, ChannelModule.PASSTHROUGH);
    }
};

DrinkModule.prototype.onMediaChange = function () {
    this.drinks = 0;
    this.arts = 0;
    this.channel.broadcastAll("drinkCount", 0);
    this.channel.broadcastAll("artCount", 0);
};

// Арбуз, Арбуза, Арбузов
DrinkModule.prototype.pluralForm = function (count, form1, form2, form3) {
	var c10 = count % 10, c100 = count % 100;
	if (c10 == 1 && c100 != 11) return form1;
	if (c10 >= 2 && c10 <= 4 && (c100 < 10 || c100 >= 20)) return form2;
	return form3;
}

module.exports = DrinkModule;
